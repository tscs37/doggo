package doggo

import "github.com/Sirupsen/logrus"

var logger = logrus.New()
var log *logrus.Entry

func init() {
	logger.Level = logrus.FatalLevel
	log = logger.WithField("source", "tscs37/doggo")
}

func EnableDebug() {
	logger.Level = logrus.DebugLevel
}
