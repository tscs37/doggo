package template

import (
	"encoding/base64"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
)

var base = "../../template"

// NotFound signals that a file has not been found
var NotFound = errors.New("File not found")

// LoadEmbeddedFile will load a file only from the asset storage
func LoadEmbeddedFile(filename string) ([]byte, error) {
	if val, ok := data[filepath.Join(base, filename)]; ok {
		return base64.RawStdEncoding.DecodeString(val)
	}
	return []byte{}, NotFound
}

var disableLocalFS = true

// DisableLocalFS will disable the local FS lookup for LoadFile
func DisableLocalFS() { disableLocalFS = true }
// EnableLocalFS will re-enable the local FS lookup for LoadFile
func EnableLocalFS()  { disableLocalFS = false }

// Load File will first look for the file on the local filesytem
// The filepath will be relative to the asset base without leading "./"
func LoadFile(file string) ([]byte, error) {
	if _, err := os.Stat(filepath.Join(base, file)); os.IsNotExist(err) && !disableLocalFS {
		return ioutil.ReadFile(filepath.Join(file))
	}
	return LoadEmbeddedFile(file)
}
