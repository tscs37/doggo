package main

import "bitbucket.org/tscs37/doggo"
import "bitbucket.org/tscs37/doggo/cmd/doggo-test/asset"

func main() {
	asset.DisableLocalFS()
	err := doggo.SetBase("./asset")
	if err != nil {
		println(err)
		return
	}
	txt, err := asset.LoadFile("some-text.txt")
	if err != nil {
		println(err)
		return
	}
	println(string(txt))
}
