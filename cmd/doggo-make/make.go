package main

import (
	"bitbucket.org/tscs37/doggo"
	"github.com/Sirupsen/logrus"
)

func main() {
	doggo.EnableDebug()
	doggo.EnableLocalFS()
	logrus.Info("Setting Template Base")
	err := doggo.SetBase("../../template")
	if err != nil {
		logrus.Info("Error: ", err)
		return
	}
	logrus.Info("OK!")
}
