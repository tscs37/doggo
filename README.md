# doggo

Doggo is an embedding engine for Go.

It embeds files in a directory into a Base64 Encoded Map
and creates a package out of that.

If the file exists in the current file system, it will preferably
load them from there.

Unlike other embedding engines, it does not rely on go-generate but instead
of embedded templates (embedded via doggo) and allows content to be replaced
by custom data if the user places the files in the filesystem.

## Why you should use doggo

You shouldn't. It's horrible and untested. But it solves a problem I have, namely not having
to use go generate for the sake of convenience.

Though on "untested"; the code is small enough to reason about without immediately suffering
a mental breakdown.

The library also relies on the developer running the binary in such a way that it can find the
static assets and properly generate the files `asset.go` and `reader.go`

## Purpose

A small webserver that needs to serve some static files.

Embedded templates.

Something. I dunno.

## Usage

### Bootstrap

As a first step, the system must be bootstrapped to generate the packages
before they can be used.

To do this, simple use `doggo.SetBase` to set the base filepath for
your assets.

```
package main

import "bitbucket.org/tscs37/doggo"

func main() {
	err := doggo.SetBase("./asset")
    if err != nil {
        println(err)
    }
}
```

When the program is next run, Doggo will generate the asset.go file, containing
the file system from that moment.

### Loading files

You can see an example in /cmd/doggo-test

```
package main

import "bitbucket.org/tscs37/doggo"
import "bitbucket.org/tscs37/doggo/cmd/doggo-test/asset"

func main() {
	// This allows doggo to only access the internal assets
	asset.DisableLocalFS()
	err := doggo.SetBase("./asset")
	if err != nil {
		println(err)
		return
	}
	txt, err := asset.LoadFile("some-text.txt")
	if err != nil {
		println(err)
		return
	}
	println(string(txt))
}
```

Files are referred to relative to the filebase without leading "./"