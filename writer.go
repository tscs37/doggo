package doggo

import (
	"encoding/base64"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"

	dg "bitbucket.org/tscs37/doggo/template"
)

var base = "."

// BaseDoesNotExist is returned by SetBase if it can't find the base on disk.
var BaseDoesNotExist = errors.New("Base does not exist")

// SetBase checks if the filebase is a directory and if yes
// generate the asset files from it.
// If SetBase fails with BaseDoesNotExist, the assets can still be loaded
// if they have been previously generated.
func SetBase(filebase string) error {
	log.Debug("Checking if file exists")
	if info, err := os.Stat(filebase); os.IsNotExist(err) {
		log.Debug("File does not exist")
		return BaseDoesNotExist
	} else if !info.IsDir() {
		log.Debug("Base is not a Directory")
		return errors.New("Base is not a directory")
	}
	log.Debug("Base Exists and is not a Directory, setting base")
	base = filebase
	log.Debug("Creating asset.go")
	err := createAssetGo(filepath.Join(base, "asset.go"), filepath.Base(filebase))
	if err != nil {
		return err
	}
	log.Debug("Creating reader.go")
	err = createReaderGo(filepath.Join(base, "reader.go"), filepath.Base(filebase))
	if err != nil {
		return err
	}
	return nil
}

// DisableLocalFS will disable loading the internal asset files from disk
var DisableLocalFS = dg.DisableLocalFS

// EnableLocalFS will enable loading the internal asset files form disk
var EnableLocalFS = dg.EnableLocalFS

var pTmpl = template.New("doggo")

func init() {
	tmpl, err := dg.LoadFile("asset.go.tmpl")
	if err != nil {
		panic(err)
	}
	pTmpl.New("doggoasset").Parse(string(tmpl))
	tmpl, err = dg.LoadFile("reader.go.tmpl")
	if err != nil {
		panic(err)
	}
	pTmpl.New("doggordr").Parse(string(tmpl))
}

func getFromBase() (map[string]string, error) {
	log.Debugf("Beginning FS Walk @ '%s'", base)
	var mapping = map[string]string{}
	walkErr := filepath.Walk(base, func(path string, info os.FileInfo, err error) error {
		log.Debugf("Walking over '%s' (isDir:%v) @ %s", info.Name(), info.IsDir(), path)
		if err != nil {
			return errors.New(err.Error())
		}
		if info.Name() == "asset.go" || info.Name() == "reader.go" || info.IsDir() {
			log.Debugf("Skipping over '%s'", info.Name())
			return nil
		}
		log.Debugf("Reading File '%s'", info.Name())
		dat, err := ioutil.ReadFile(path)
		if err != nil {
			log.Debugf("Error reading file '%s': %s", info.Name())
			return err
		}
		log.Debugf("Read %d bytes from '%s'", len(dat), info.Name())
		mapping[path] = base64.RawStdEncoding.EncodeToString(dat)
		return nil
	})
	return mapping, walkErr
}

func createAssetGo(dest string, packagename string) error {
	log.Debugf("Scanning Base @ '%s'", base)
	data, err := getFromBase()
	if err != nil {
		log.Debugf("Error while scanning base: %s", err)
		return err
	}
	log.Debugf("Got %d entries", len(data))
	var meta = struct {
		GoPack string
		Assets map[string]string
	}{
		GoPack: packagename,
		Assets: data,
	}
	log.Debugf("Writing to file...")
	f, err := os.OpenFile(dest, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	defer f.Close()
	if err != nil {
		log.Debugf("Error on file: %s", err)
		return err
	}
	log.Debugf("Executing Template")
	return pTmpl.ExecuteTemplate(f, "doggoasset", meta)
}

func createReaderGo(dest string, packagename string) error {
	var meta = struct {
		GoPack string
		Base   string
	}{
		GoPack: packagename,
		Base:   base,
	}
	log.Debugf("Writing to file...")
	f, err := os.OpenFile(dest, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	defer f.Close()
	if err != nil {
		log.Debugf("Error on file: %s", err)
	}
	log.Debugf("Executing Reader Template")
	return pTmpl.ExecuteTemplate(f, "doggordr", meta)
}
